function! ConfigureView()
    set encoding=utf-8
    set number
    syntax on
    colorscheme monokai
    set incsearch
    set hlsearch
    set ignorecase
    set smartcase
    set termencoding=utf8
    set nocompatible
    set ruler
    set foldenable
    set foldlevel=100
    set foldmethod=indent
    set noerrorbells visualbell t_vb=
    autocmd GUIEnter * set visualbell t_vb=
    set mouse=a
    set mousemodel=popup
    set hidden
    set ch=1
    set mousehide
    set autoindent
    set expandtab
    set shiftwidth=2
    set softtabstop=2
    set tabstop=2
    set statusline=%<%f%h%m%r\ %b\ %{&encoding}\ 0x\ \ %l,%c%V\ %P
    set laststatus=2
    set smartindent
    set showmatch
    " Навигация с учетом русских символов
    set iskeyword=@,48-57,_,192-255
    set backspace=indent,eol,start
    set history=200
    set wildmenu
    set list listchars=tab:→\ ,trail:·
    filetype plugin off
endfunc

call ConfigureView()

set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'valloric/youcompleteme'
Plugin 'mattn/emmet-vim'
Plugin 'scrooloose/nerdtree'
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/syntastic'
Plugin 'airblade/vim-gitgutter'
Plugin 'tpope/vim-rails'
Plugin 'elixir-lang/vim-elixir'
Plugin 'jiangmiao/auto-pairs'
Plugin 'bling/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'vim-ruby/vim-ruby'
Plugin 'sickill/vim-monokai'

call vundle#end()
filetype plugin indent on

let g:airline_theme='wombat'

map <C-n> :NERDTreeToggle<CR>