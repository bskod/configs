#!/usr/bin/env bash

GREEN='\033[00;32m'
APPNAME='vagrant'

function install {
    echo -e "${GREEN}Installing =>" $1
    shift
    apt-get -y install "$@" >/dev/null 2>&1
}

echo adding swap file
fallocate -l 2G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo '/swapfile none swap defaults 0 0' >> /etc/fstab

echo -e "${GREEN}Updating repos"
wget --quiet -O - https://deb.nodesource.com/setup_14.x | sudo -E bash - >/dev/null 2>&1
wget --quiet -O - https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add - >/dev/null 2>&1
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list >/dev/null 2>&1
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - >/dev/null 2>&1
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list' >/dev/null 2>&1
wget --quiet -O - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add - >/dev/null 2>&1
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list >/dev/null 2>&1
apt-get -y update >/dev/null 2>&1

echo -e "${GREEN}Upgrading the system"
apt-get -y upgrade >/dev/null 2>&1

install 'Development Tools' autoconf git-core nginx bison build-essential libssl-dev libtool libyaml-dev libreadline-dev zlib1g-dev libncurses-dev libffi-dev libgdbm-dev
install 'Frontend Tools' nodejs yarn
install 'PostgreSQL' postgresql postgresql-contrib libpq-dev
install 'Elasticsearch' elasticsearch

echo -e "${GREEN}Installing => rbenv and ruby-build"
sudo su - ${APPNAME} -c "git clone https://github.com/rbenv/rbenv.git /home/${APPNAME}/.rbenv"
sudo su - ${APPNAME} -c "git clone https://github.com/rbenv/ruby-build.git /home/${APPNAME}/.rbenv/plugins/ruby-build"

echo -e "${GREEN}Exporting rbenv and ruby-build into bashrc"
echo -e "export PATH=\"/home/${APPNAME}/.rbenv/bin:/home/${APPNAME}/.rbenv/plugins/ruby-build/bin:\$PATH\"" >> /home/${APPNAME}/.bashrc
echo 'eval "$(rbenv init -)"' >> /home/${APPNAME}/.bashrc
sudo -H -u ${APPNAME} bash -i -c "rbenv install 2.7.2 && rbenv global 2.7.2 && ruby -v && rbenv rehash"
echo -e "${GREEN}Installing => RubyGems"
sudo -H -u ${APPNAME} bash -i -c "gem update --system -N" >/dev/null 2>&1
echo -e "${GREEN}Installing => Bundler"
sudo -H -u ${APPNAME} bash -i -c "gem install bundler -N && rbenv rehash"
sudo -H -u ${APPNAME} bash -i -c "bundler -v"

echo -e "${GREEN}======================="
echo -e "${GREEN}== all set, rock on! =="
echo -e "${GREEN}======================="
